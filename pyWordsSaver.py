import tkinter as tk

class WordsSaver():
    def __init__(self, master):
        # Settings
        self.master = master
        master.title("Words Saver")
        master.resizable(False, False)

        # Variables
        self.new_words = 0

        # Widgets
        self.text_to_save = tk.Text(master, width=30, height=5)
        self.text_to_save.grid(column=0, row=0, padx=10, pady=10)

        self.save_button = tk.Button(master, text="Save")
        self.save_button.bind("<Button-1>", self.save_to_file)
        self.save_button.grid(column=0, row=1, padx=10, pady=10)

        master.bind("<Return>", self.save_to_file)

        self.all_words_stat = tk.Label(master, text=f"Total words: {self.count_words_in_file()}")
        self.all_words_stat.grid(column=0, row=2)
        self.new_words_stat = tk.Label(master, text=f"New added words: {self.new_words}")
        self.new_words_stat.grid(column=0, row=3)


    # Methods
    def save_to_file(self, e):
        # save word to file and then clear text field
        text = self.text_to_save.get("1.0", "end-1c").strip() + "\n"
        if len(text) > 1:
            print(len(text))
            with open("saved_words.txt", "a") as file:
                file.write(text)
        self.text_to_save.delete("1.0", "end")

        # update statistics
        self.new_words += 1
        self.all_words_stat["text"] = f"Total words: {self.count_words_in_file()}" # refresh words count
        self.new_words_stat["text"] = f"New added words: {self.new_words}" # refresh words count

    def count_words_in_file(self):
        # count current words in file
        count = 0
        with open("saved_words.txt", "r") as file:
            for line in file:
                line = line.strip("\n")
                count += len(line.split())
        return count

def main():
    root = tk.Tk()
    app = WordsSaver(root)
    root.lift()
    root.mainloop()

if __name__ == '__main__':
    main()

